process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = '0';
const express = require('express');
const bodyParser = require('body-parser');
const Agent = require('agentkeepalive');
const fs = require('fs');
const path = require('path');
const https = require('https');
const qs = require('qs');
const axios = require('axios');
const cors = require('cors');
const request = require('request-promise');
const app = express();
app.use(cors());
const port = process.env.PORT || 4000;
const keepaliveAgent = new Agent({
    maxSockets: 100,
    maxFreeSockets: 10,
    timeout: 60000, // active socket keepalive for 60 seconds
    freeSocketTimeout: 30000, // free socket keepalive for 30 seconds
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/api/hello', (req, res) => {
    res.send({ express: 'Hello From Express' });
});

app.post('/access/ticket', async (req, res) => {
    console.log(req.body.formData);
    const { username, password, realm } = req.body.formData
    let dataString = `username=${username}@${realm}&password=${password}`;
    console.log(dataString, typeof (dataString))
    const Login = async () => {
        try {
            return await axios({
                method: 'post',
                url: 'https://192.168.111.2:8006/api2/extjs/access/ticket',
                rejectUnauthorized: false,
                agent: false,
                timeout: 300000,
                data: dataString,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
                }
            })

        }

        catch (error) {
            console.log(error);
        }
    }
    let response = await Login();
    console.log("Response to send back", response.data);
    res.send(response.data);
});
app.post('/node/lxc', async (req, res) => {
    let data = req.body.formData;
    console.log("Data recieved from client", data, typeof (data));
    let cookies = req.body.Cookies;
    console.log("Cookies recieved from client", cookies, typeof (cookies));

    const CreateLXC = async () => {
        console.log("Vmid of data is", data.vmid);
        try {
            return await axios({
                method: 'post',
                url: 'https://192.168.111.2:8006/api2/extjs/nodes/vnode1/lxc',
                rejectUnauthorized: false,
                agent: false,
                timeout: 300000,
                data: qs.stringify({
                    vmid: data.vmid,
                    ostemplate: data.ostemplate,
                    hostname: data.hostname,
                    password: data.password,
                    storage: data.storage,
                    rootfs: data.rootfs,
                    memory: data.memory,
                    swap: data.swap,
                    cores: data.cores,
                    net0: data.net0


                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
                    'Content-Length': JSON.stringify(data).length,
                    'CSRFPreventionToken': cookies.CSRFPreventionToken,
                    'Cookie': cookies.Cookie
                }
            })

        }

        catch (error) {
            console.log(error);
        }
    }
    let response = await CreateLXC();
    console.log(response.data.success);
    if (response.data.success === 1) {
        res.status(200).send("LXC Container created Successfully!");
    }
    else {
        (res.send("Error in creating lxc container!"));
    }
});

if (process.env.NODE_ENV === 'production') {
    // Serve any static files
    app.use(express.static(path.join(__dirname, 'client/build')));

    // Handle React routing, return all requests to React app
    app.get('*', function (req, res) {
        res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
    });
}
var server = app.listen(port, () => console.log(`Listening on port ${port}`));
server.timeout = 300000;