## For Development Environment:

1.  Clone this repo using url by running this command:

        git clone git@bitbucket.org:hamzaxoho/proxmoxia.git

2.  In the root directory of the repo first of all install all the dependencies and devdependecies by running following commands,

        npm install

        cd client && npm install

3.  To satart both reactjs and nodejs servers concurrently in development Run,

         npm run dev

    This help to run both node/express and reactjs servers concurrently.
