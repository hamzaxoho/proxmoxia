import React, { Component } from 'react';
import Form from "react-jsonschema-form";
import { read_cookie } from 'sfcookies';
const uiSchema = {
    "password": {
        "ui:widget": "password",
        "ui:help": "Hint: Make it strong!"
    },
    "net0": {
        "ui:help": "Hint: name=<name>,bridge=<bridge>,ip=<youripv4/CIDR>,gw=<yourgateway>,firewall=<0|1>"

    }, "rootfs": {
        "ui:help": "Hint:local-lvm:8(inGB)"

    }, "ostemplate": {

        "ui:help": " Hint: iso_bank:vztmpl/debian-9.0-standard_9.7-1_amd64.tar.gz"

    }



};
const schema = {
    "type": "object",
    "properties": {
        "vmid": {
            "type": "integer",
            "minLength": 3,
            "title": "Vmid",
            "maxLength": 255
        },
        "ostemplate": {
            "type": "string",
            "title": "OSTemplate"
        },
        "net0": {
            "title": "Net0",
            "type": "string"
        },
        "hostname": {
            "title": "Hostname",
            "type": "string"
        },
        "password": {
            "title": "Password",
            "type": "string"
        },
        "rootfs": {
            "title": "Rootfs",
            "type": "string"
        },
        "ssh-public-keys": {
            "title": "Ssh Public Key",
            "type": "string"
        },
        "storage": {
            "title": "Storage",
            "type": "string"
        },
        "memory": {
            "title": "Memory",
            "type": "integer"
        },
        "swap": {
            "title": "Swap",
            "type": "integer"
        },
        "cores": {
            "title": "Cores",
            "type": "integer"
        },
        "unprivileged": {
            "title": "Unprivileged",
            "type": "boolean"
        },
        "start": {
            "title": "Start",
            "type": "boolean"
        }
    },
    "required": [
        "vmid",
        "ostemplate"
    ]
}
const log = (type) => console.log.bind(console, type);
export default class LxcContainer extends Component {
    state = {
        success: false,
        response: '',
        post: '',
        responseToPost: ''
    };
    handleSubmit = async ({ formData }) => {
        console.log("Data submitted: ", formData);
        let Cookies = {
            CSRFPreventionToken: read_cookie('CSRFPreventionToken'),
            Cookie: read_cookie('Cookie')
        }
        console.log(read_cookie('CSRFPreventionToken'));
        console.log(read_cookie('Cookie'));
        console.log("Cookies object", Cookies);
        let data = JSON.stringify({ formData: formData, Cookies: Cookies });
        console.log("Data Contained", data);
        const response = await fetch('/node/lxc', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: data,
        });
        const body = await response.text();
        console.log(body);
        if (body !== null) {
            this.setState({ responseToPost: body, success: true });
        }
        if (this.state.success === true) {
            alert(this.state.responseToPost);
        }
    };
    render() {
        return (
            <div>
                <h2>Add a new Container!</h2>
                <Form schema={schema}
                    uiSchema={uiSchema}
                    onSubmit={this.handleSubmit}
                    onError={log("errors")} />

            </div>
        )
    }
}
