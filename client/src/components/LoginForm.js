import React, { Component } from 'react';
import Form from "react-jsonschema-form";
import 'bootstrap/dist/css/bootstrap.min.css';
import { bake_cookie } from 'sfcookies';
const schema = {
    "title": "Login Form",
    "type": "object",
    "required": ["username", "password"],
    "properties": {
        "username": {
            "type": "string",
            "title": "UserName",
            "default": "hamza.khalid"
        },
        "password": {
            "type": "string",
            "title": "Password",
            "minLength": 3
        },
        "realm": {
            "type": "string",
            "title": "Realm",
            "default": "xohotech.net"
        }
    }
};
const uiSchema = {
    "password": {
        "ui:widget": "password",
        "ui:help": "Hint: Make it strong!"
    }
};
const log = (type) => console.log.bind(console, type);

export default class LoginForm extends Component {
    state = {
        success: false,
        response: '',
        post: '',
        responseToPost: []


    };
    componentDidMount() {
        this.callApi()
            .then(res => this.setState({ response: res.express }))
            .catch(err => console.log(err));
    }

    callApi = async () => {
        const response = await fetch('/api/hello');
        const body = await response.json();
        if (response.status !== 200) throw Error(body.message);

        return body;
    }
    handleSubmit = async ({ formData }) => {
        console.log("Data submitted: ", formData);
        const response = await fetch('/access/ticket', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({ formData: formData }),
        });
        const body = await response.json();
        console.log(body.data.CSRFPreventionToken);
        console.log(body.data.ticket);
        this.setState({ responseToPost: body, success: true });
        if (this.state.success === true) {
            bake_cookie('CSRFPreventionToken', body.data.CSRFPreventionToken);
            bake_cookie('Cookie', 'PVEAuthCookie=' + body.data.ticket);
            this.props.history.push('/addcontainer');
        }
        else {
            this.props.history.push('/');
        }
    };
    render() {

        return (
            <div>
                <p>{this.state.response}</p>
                <Form schema={schema}
                    uiSchema={uiSchema}
                    onSubmit={this.handleSubmit}
                    onError={log("errors")} />

            </div>
        )
    }
}
