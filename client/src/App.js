import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LoginForm from './components/LoginForm';
import LxcContainer from './components/LxcContainer';

export default class App extends Component {
  render() {
    return (
      <div className="App container">
        <h1> Welcome to Proxmoxia VE</h1>
        <Router>
          <Switch>
            <Route exact path="/" component={LoginForm} />
            <Route exact path="/addcontainer" component={LxcContainer} />
          </Switch>

        </Router>
      </div>
    )
  }
}
